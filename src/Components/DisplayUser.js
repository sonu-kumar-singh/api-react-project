import React from 'react'
import './DisplayUser.css'
export default function DisplayUser(props) {
  return (
    <div className='DisplayUser'>
        <label className='Name'>Name: {props.user.username}</label>
        <div className='Address'>
          <label>Address: </label>
          <span>{props.user.address.street},</span>
          <span>{props.user.address.city},</span>
          <span>{props.user.address.zipcode}</span>
        </div>
        <label>Email: {props.user.email}</label>
        <div>Phone: {props.user.phone}</div>
       
    </div>
  )
}
