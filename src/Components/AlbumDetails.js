import React, { Component } from 'react'
import './AlbumDetails.css'
export default class AlbumDetails extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         albums: ''
      }
    }

    componentDidMount(){
        let {id} = this.props
        fetch(`https://jsonplaceholder.typicode.com/albums?userId=${id}`)
        .then(res => res.json())
        .then(res => {this.setState({albums: res})})
    }
  render() {
    let{albums} = this.state
   
    if(albums.length>0){
      return (
        <div>
            <label className='AlbumHeader'>Album</label><br/>
            <label className='AlbumTitle'>Title</label><br/>
            <div className='AlbumContent'>{albums[0].title}</div>
        </div>
     )
    }else{
      return null;
    }
    
  }
}
