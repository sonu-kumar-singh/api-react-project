import React, { Component } from 'react'
import './PhotoDetails.css'
export default class PhotoDetails extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         photosUrl: []
      }
    }
    componentDidMount(){
        let {id}=this.props;
        fetch(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`)
        .then(res => res.json())
        .then(res => {this.setState({photosUrl: res})})
    }
  render() {
    let {photosUrl}=this.state
    if(photosUrl.length>0){
        let firstFiveUrls = photosUrl.slice(0,5);
            return (
                firstFiveUrls.map(photoInfo => {
                 return <img src={photoInfo.url} alt='album-pic'></img>
                })
              )
        
    }else{
        return null;
    }
    
  }
}
