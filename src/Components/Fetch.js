
import React, { Component } from 'react'
//import fetch from 'node-fetch';
import DisplayUser from './DisplayUser'
import PostDetails from './PostDetails'
import AlbumDetails from './AlbumDetails'
import PhotoDetails from './PhotoDetails'
import './Fetch.css'
export default class Fetch extends Component {
    constructor(props) {
      super(props)
    this.state = {
        data: []
        }
      
    }
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then(res => {this.setState({data: res})}) 
    }
  render() {
    let {data}=this.state
    return (
            data.map(user=>{
              return <div>
                         <DisplayUser user={user}/>
                         <PostDetails id={user.id}/>
                         <AlbumDetails id={user.id}/>
                         <div className='PhotoContainer'><PhotoDetails id={user.id}/></div>
                    </div>
                
              })
        )
    
  }
}
