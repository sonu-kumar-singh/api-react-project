import React, { Component } from 'react'
import './PostDetails.css'
 class PostDetails extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
         posts:[]
      }
    }
    componentDidMount(){
        let {id}=this.props
        //console.log('Id is ',id)
        fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
        .then(res => res.json())
        .then(res => {this.setState({posts: res})})
        //fetch(`{$}`)
    }
  render() {
    //console.log(this.state.post)
    let {posts}=this.state
    //console.log(post)
    return (
        <div>
            <div className='postHeader'>Posts</div>
                {posts.map((post) => {
                        return (
                            <div className='eachPost'>
                                <label className='labelHeader'>Title</label>
                                <div className='postContent'>{post.title}</div> 
                                <label className='labelHeader'>Body</label>
                                <div className='postContent'>{post.body}</div>
                            </div>
                        )
                        
                    })
                }
        </div>

    )
   
  }
}

export default PostDetails